;;;; file-converter.lisp

(in-package :file-converter)



(defun build-cmd (from-type to-type source-file output-file)
  (let* ((out-file (if output-file output-file source-file))
         (cmd (format nil "pandoc '~a' -f ~a -o '~a' -t ~a"
                      (make-filename source-file from-type)
                      from-type
                      (make-filename out-file to-type)
                      to-type)))
      cmd))

(defun run-pandoc (&rest args)
  (uiop:run-program (apply #'build-cmd args)))


(defun convert (from-type to-type source-file &optional output-file)
  "Convert between file type

  fromp-type and to-type are strings that must be found in the car position of
  the *file-extensions* table. That list is taken from the following URL

  URL https://pandoc.org/MANUAL.html#general-options

  Extension is inferred by the types mentioned at the url"
  (run-pandoc from-type to-type source-file output-file))
