(in-package :file-converter)

(defparameter *file-types* '(md org html))

;;; URL: https://pandoc.org/MANUAL.html#general-options
;;; (convert A B ...) when A or B is not found, it assumes that A|B is also
;;; the extension for that file type, and so is not necessary to be added to
;;; this table.
;;; PDF is not present as it requires several external dependices and does not
;;; seem to be generalizable.
(defparameter *file-extensions* '(("markdown"           "md")
                                  ("markdown_mmd"       "md")
                                  ("markdown_phpextra"  "md")
                                  ("markdown_strict"    "md")
                                  ("commonmark"         "md")
                                  ("commonmark_x"       "md")
                                  ("gfm"                "md")
                                  ("org"                "org")
                                  ("html"               "html")
                                  ("latex"              "tex")))

(defparameter *path-delimeter* (uiop:directory-separator-for-host))
