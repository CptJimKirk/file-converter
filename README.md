# File-Converter

A Common Lisp wrapper for a subset of [PANDOC](http://pandoc.org). This
library has been tested to convert between:

`org html md latex json`

To use this library, you will need to [INSTALL
PANDOC](http://pandoc.org/installing.html).

1.  Clone This Repository into your `quicklisp/local-projects` directory
2.  Open Emacs
3.  Navigate to this file
4.  `M-x slime`
5.  Run the code block below
6.  `NIL` is output as there is no return value.
7.  Check that `./README.html` exists
8.  Check its contents

<!-- end list -->

``` commonlisp
(ql:quickload "file-converter")
(file-converter:convert "org" "html" "./README")
```

A 5th argument `to-file` is `&optional`. If omitted, a file with the
same name, but new extension will be created.

**NOTE:** A relative or absolute path is required

Available types are in the `car` position of `*file-extensions*` found
in `./definitions.lisp`. The [PANDOC
MANUAL](https://pandoc.org/MANUAL.html#general-options) contains
documentation for the remaining types, but only the types found in
`./definitions.lisp` have been tested. Please report any bugs as by
clicking `issues` in this repository.

### The README.md that you're reading was created using this library

``` commonlisp
(convert "org" "gfm" "./README")
```

This example uses `org` to `gfm` which stands for `Github Flavored
Markdown` because the standard markdown parser for `pandoc` produces
tables incompatible with github style .md files. If you were to run

``` commonlisp
(convert "org" "markdown" "./README")
```

The following table would appear jumbled and unintelligible.

| Just | An                       | Example  |
| ---- | ------------------------ | -------- |
| ASDF | Another Steely Dan Fan   | 18-24    |
| FFA  | Free From Advertisements | 13-60-20 |
| LOL  | Laugh of Legends         | 1-2-345  |
