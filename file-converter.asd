;;;; file-converter.asd

(asdf:defsystem #:file-converter
  :description "use pandoc to convert between file formats"
  :author "cpt.jim.kirk@proton.me"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on ("str" "uiop" "ppath")
  :components ((:file "package")
               (:file "utility")
               (:file "definitions")
               (:file "file-converter")))
