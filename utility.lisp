(in-package :file-converter)


(defun symb->str (s)
  (string-downcase (write-to-string s)))

(defun tail (string)
  "/path/to/file.txt -> file.txt"
  (ppath:basename string))

(defun head (string)
  "/path/to/file.txt -> /path/to"
  (ppath:dirname string))

(defun get-ext (key)
  "match the key in the file-extensions"
  (let ((pair (assoc key *file-extensions* :test #'string-equal)))
    (if pair
        (second pair)
        key)))

(defun add-ext(name file-type)
  "append the filename with its extension"
  (format nil "~a.~a" name (get-ext file-type)))

(defun at-least-1 (lst)
  (let ((but-last (butlast lst)))
    (if but-last but-last lst)))

(defun rem-ext (pathname)
  "remove the extension from the filename"
  (str:join *path-delimeter*
            (list (head pathname)
                  (str:join "." (at-least-1 (str:split "." (tail pathname)))))))

(defun make-filename (pathname file-type)
  (add-ext (rem-ext pathname) file-type))
